package S50TASK5250;
import java.util.Date;
import java.util.Locale;
import java.util.Arrays;
import java.text.NumberFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
public class Order {
    int id; //id order
    String customerName; //ten khach hang
    long price; //tong gia tien
    Date orderDate; // ngay thuc hien order
    boolean confirm; // da xac nhan hay chua
    String[] items; //danh sach mat hang da mua
    //khoi tao voi mot tham so customerName
    public Order(String customerName) {
        this.id = 1;
        this.customerName = customerName;
        this.price = 150000;
        this.orderDate = new Date();
        this.confirm = true;
        this.items = new String[] {"Book", "pen", "rule"};
    }

    // khoi tao voi tat ca tham so
    public Order(int id, String customerName, long price, Date orderDate, boolean confirm, String[] items) {
        this.id = id;
        this.customerName = customerName;
        this.price = price;
        this.orderDate = orderDate;
        this.confirm = true;
        this.items = new String[] {"Book", "pen", "rule"};
    }

    //khoi tao khong tham so
    public Order() {
        this("2");
    }
    //khoi tao voi 3 tham so
    public Order(int id, String customerName, long price) {
        this(id, customerName, price, new Date(), true, new String[] {"Book", "pen", "rule"});
    }

    @Override
    public String toString() {
        //dinh dang tieu chuan viet nam
        Locale.setDefault(new Locale("vi", "VN"));
        //dinh dang ngay thang
        String pattern = "dd-MMMM-yyyy HH:mm:ss.SSS";
        DateTimeFormatter defautTimeFormatter = DateTimeFormatter.ofPattern(pattern);
        //dinh dang cho gia tien
        Locale usLocale = Locale.getDefault();
        NumberFormat usNumberFormat = NumberFormat.getCurrencyInstance(usLocale);
        //tra ve chuoi
        return 
        "Order [id=" + id 
        + ", customerName=" + customerName 
        + ", price=" + usNumberFormat.format(price)
        + ", orderDate=" + defautTimeFormatter.format(orderDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime()) 
        + ", confirm=" + confirm 
        + ", items=" + Arrays.toString(items) + "]";
    }

}
