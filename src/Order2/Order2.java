package Order2;

import java.text.NumberFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.time.LocalDate;
import Task5230.Person;

public class Order2 {
    int id;
    String customer;
    Integer price;
    LocalDate orderDate;
    Boolean confirm;
    String[] items;
    Person buyer;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public LocalDate getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        ;
    }

    public Boolean getConfirm() {
        return confirm;
    }

    public void setConfirm(Boolean confirm) {
        this.confirm = confirm;
    }

    public String[] getItems() {
        return items;
    }

    public void setItems(String[] items) {
        this.items = items;
    }

    public Person getBuyer() {
        return buyer;
    }

    public void setBuyer(Person buyer) {
        this.buyer = buyer;
    }

    // khoi tao khong tham so
    public Order2() {
    }

    // khoi tao voi 1 tham so
    public Order2(String customer) {
        this.customer = customer;
    }

    // khoi tao voi tat ca tham so
    public Order2(int id, String customer, Integer price, LocalDate date, Boolean confirm, String[] items,
            Person buyer) {
        this.id = id;
        this.customer = customer;
        this.price = price;
        this.orderDate = date;
        this.confirm = confirm;
        this.items = items;
        this.buyer = buyer;
    }
    //khoi tao voi 3 tham so
    public Order2(int id, String customer, Integer price) {
        this.id = id;
        this.customer = customer;
        this.price = price;
    }

    @Override
    public String toString() {
        Locale.setDefault(new Locale("vi", "VN"));

        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance();
        String formattedOrderDate = orderDate != null ? dateFormatter.format(orderDate) : ""; // Kiểm tra null trước khi định dạng
        String formattedPrice = price != null ? currencyFormatter.format(price) : ""; // Kiểm tra null trước khi định
                                                                                      // dạng

        return "Order2 [id=" + id + ", customer=" + customer + ", price=" + formattedPrice
                + ", orderDate=" + formattedOrderDate + ", confirm=" + confirm + ", items="
                + Arrays.toString(items) + ", buyer=" + buyer + "]";
    }

}
