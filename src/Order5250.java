import S50TASK5250.Order;
import java.util.Date;
import java.util.ArrayList;
public class Order5250 {
    public static void main(final String[] args) throws Exception{
        final ArrayList<Order> arrayList = new ArrayList<Order>();
        // khoi tao order voi cac tham so khac nhau (khoi tao 4 obj order)
        Order Order1 = new Order();
        Order Order2 = new Order("Lan");
        Order Order3 = new Order(3, "Long", 80000);
        Order Order4 = new Order(4, "Nam", 75000, new Date(), false, new String[] {"hop mau", "tay", "giay mau"});

        arrayList.add(Order1);
        arrayList.add(Order2);
        arrayList.add(Order3);
        arrayList.add(Order4);

        //in ket qua ra console theo dinh dang tieu chuan viet nam gia tien, ngay thang
        for (Order order : arrayList) {
            System.out.println(order.toString());
        }
    }

}
