import java.util.ArrayList;
import Task5230.Person;

public class st5230 {
    public static void main(String[] args) throws Exception {
        ArrayList<Person> arrListPerson = new  ArrayList<Person>();
        //Khởi tạo person với các tham số khác nhau
        Person person0 = new Person();
        Person person1 = new Person("devcamp");
        Person person2 = new Person("Viet", 24, 66.8);
        Person person3 = new Person("Nam", 30, 68.8, 5000000, new String[] {"Little Camel"});
        //them object person vào list
        arrListPerson.add(person0);
        arrListPerson.add(person1);
        arrListPerson.add(person2);
        arrListPerson.add(person3);
        //in ra màng hình
        for (Person person : arrListPerson) {
            System.out.println(person);
        }
    }
}
