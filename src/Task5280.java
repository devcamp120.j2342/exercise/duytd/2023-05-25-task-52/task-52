import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Random;

public class Task5280 {

    public static void main(String[] args) {
        double number = 2.100212;
        int scale = 2;
        String roundedNumber = round(number, scale);
        System.out.println("Số sau khi làm tròn: " + roundedNumber);
        double number2 = 2.100212;
        int scale2 = 3;
        String roundedNumber2 = round(number2, scale2);
        System.out.println("Số sau khi làm tròn: " + roundedNumber2);

        double number3 = 2100;
        int scale3 = 2;
        String roundedNumber3 = round(number3, scale3);
        System.out.println("Số sau khi làm tròn: " + roundedNumber3);

        System.out.println("st2");
        int min = 1;
        int max = 10;
        int randomNumber = getRandomNumber(min, max);
        int min2 = 10;
        int max2 = 20;
        int randomNumber2 = getRandomNumber(min2, max2);
        System.out.println("Số ngẫu nhiên: " + randomNumber);
        System.out.println("Số ngẫu nhiên: " + randomNumber2);

        System.out.println("st3");
        double a = 2.0;
        double b = 4.0;

        double c = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));

        System.out.println("Cạnh huyền của tam giác vuông là: " + c);

        double a2 = 3.0;
        double b2 = 4.0;

        double c2 = Math.sqrt(Math.pow(a2, 2) + Math.pow(b2, 2));

        System.out.println("Cạnh huyền của tam giác vuông là: " + c2);

        System.out.println("st4");
        int b1 = 64;
        int b22 = 94;
        boolean valiDateb1 = soChinhPhuong(b1);
        boolean valiDateb2 = soChinhPhuong(b22);
        System.out.println(valiDateb1);
        System.out.println(valiDateb2);

        System.out.println("st5");
        int e1 = 32;
        int e2 = 137;

        int soLonHone1 = soChiaHetCho5(e1);
        int soLonHone2 = soChiaHetCho5(e2);

        System.out.println(soLonHone1);
        System.out.println(soLonHone2);

        System.out.println("st6");
        String st61 = "abc";
        String st62 = "12";
        boolean ResultSt61 = isNumeric(st61);
        boolean ResultSt62 = isNumeric(st62);
        System.out.println(ResultSt61);
        System.out.println(ResultSt62);

        System.out.println("st7");
        int st71 = 16;
        int st72 = 18;
        int st73 = 256;

        boolean luyThuaCua2st71 = valiDateSt7(st71);
        boolean luyThuaCua2st72 = valiDateSt7(st72);
        boolean luyThuaCua2st73 = valiDateSt7(st73);

        System.out.println(luyThuaCua2st71);
        System.out.println(luyThuaCua2st72);
        System.out.println(luyThuaCua2st73);

        System.out.println("st8");
        int st81 = -15;
        int st82 = 1;
        double st83 = 1.2;
        
        boolean soNguyen3 = valiDateSt82(st83);
        boolean soNguyen1 = valiDateSt8(st81);
        boolean soNguyen2 = valiDateSt8(st82);

        System.out.println(soNguyen1);
        System.out.println(soNguyen2);
        System.out.println(soNguyen3);

        System.out.println("st9");
        int st91 = 1000; // Số cần thêm dấu ","
        double st92 = 10000.23;
        NumberFormat numberFormat = NumberFormat.getInstance();
        String formattedNumber = numberFormat.format(st91);
        System.out.println("Số đã được định dạng: " + formattedNumber);

        String formattedNumber2 = numberFormat.format(st92);
        System.out.println("Số đã được định dạng: " + formattedNumber2);

        System.out.println("st10");
        int st10 = 51;
        String result = Integer.toBinaryString(st10);
        System.out.println(result);

    }

    // Region method()12

    public static boolean valiDateSt82(double number) {
        if(number >= 0 && number == (int) number){
            return true;
        }else{
            return false;
        }
    }
    public static boolean valiDateSt8(int number){
        if(number >= 0){
            return true;
        }else{
            return false;
        }
    }

    public static boolean valiDateSt7(int number) {
        if(number <= 0)
        {
            return false;
        }
        while(number > 1)
        {
            if(number % 2 != 0){
                return false;
            }
            number /= 2;
        }
        return true;
    }

    public static boolean isNumeric(String str) {
        if (str == null || str.length() == 0) {
            return false;
        }
        for (char c : str.toCharArray()) {
            if (!Character.isDigit(c)) {
                return false;
            }
        }

        return true;
    }

    public static int soChiaHetCho5(int number) {
        int nearestMultiple = (number / 5) * 5; // Lấy số lớn nhất chia hết cho 5

        if (nearestMultiple < number) {
            nearestMultiple += 5; // Tăng số lên để lấy số lớn hơn gần nhất
        }
        return nearestMultiple;
    }

    public static boolean soChinhPhuong(int number) {
        // Tính căn bậc hai của số và chuyển đổi thành số nguyên
        int squareRoot = (int) Math.sqrt(number);
        // Kiểm tra nếu số mũ hai của squareRoot bằng number
        if (squareRoot * squareRoot == number) {
            return true;
        } else {
            return false;
        }
    }

    public static int getRandomNumber(int min, int max) {
        Random random = new Random();
        return random.nextInt(max - min + 1) + min;
    }

    public static String round(double number, int scale) {
        String pattern = "#.";
        for (int i = 0; i < scale; i++) {
            pattern += "0";
        }
        DecimalFormat decimalFormat = new DecimalFormat(pattern);
        return decimalFormat.format(number);
    }
}
