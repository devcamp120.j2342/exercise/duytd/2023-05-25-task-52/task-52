import Order2.Order2;
import Task5230.Person;
import java.time.LocalDate;
import java.util.ArrayList;
// import java.util.Date;

public class Order5270 {
    public static void main(String[] args) throws Exception {
        ArrayList<Order2> arrListOrder2 = new ArrayList<Order2>();
        // khoi tao order voi cac tham so khac nhau (khoi tao 4 obj order)
        Order2 order1 = new Order2();
        Order2 order2 = new Order2("duytd");
        Order2 order3 = new Order2(1, "cris", 250000);
        Order2 order4 = new Order2(1, "leo", 200000, LocalDate.now(), false, new String[]{"ram", "cpu", "pin"}, new Person("leo", 30, 61, 20000000, new String[]{"mill", "yello", "lu"}));

        arrListOrder2.add(order1);
        arrListOrder2.add(order2);
        arrListOrder2.add(order3);
        arrListOrder2.add(order4);

        //in ra console
        for (Order2 order : arrListOrder2) {
            System.out.println(order);
        }
    }
}
