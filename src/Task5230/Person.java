package Task5230;
import java.util.Arrays;
public class Person {
    //sửa lại phương thức person
    @Override
    public String toString(){
        return "Person [name=" + name + ", age=" + age + ", weight=" + weight + ", salary= " + salary + ", pets=" + Arrays.toString(pets) + "]"; 
    }

    String name;
    int age; //tuổi
    double weight; //cân nặng
    long salary; // lương
    String[] pets; //vật nuôi

    //Khởi tạo với một tham số name
    public Person(String name) {
        this.name = name;
        this.age = 18;
        this.weight = 60.5;
        this.salary = 10000000;
        this.pets = new String[] {"Big dog", "Small Cat", "Gold firsh", "Red Parrot"};

    }

    // khởi tạo với tất cả tham số
    public Person(String name, int age, double weight, long salary, String[] pets) {
        this.name = name;
        this.age = age;
        this.weight = weight;
        this.salary = salary;
        this.pets = pets;
    }

     //Khởi tạo không tham số
    public Person() {
    }

    //Khởi tạo với 3 tham số
    public Person(String name, int age, double weight) {
        this(name, age, weight, 2000000, new String[] {"Big dog", "Small Cat", "Gold firsh", "Red Parrot"});
        
    }

   
    
    
}
