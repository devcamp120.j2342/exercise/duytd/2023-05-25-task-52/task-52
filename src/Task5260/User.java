package Task5260;

public class User {
    String Password;
    String UserName;
    boolean Enabled;
    //khoi tao voi tat ca tham so
    public User(String Password, String userName, boolean enabled) {
        this.Password = Password;
        this.UserName = userName;
        this.Enabled = enabled;
    }
    //khoi tao voi khong tham so
    public User() {

    }

    public String getPassword() {
        return Password;
    }
    public void setPassword(String password) {
        Password = password;
    }
    public String getUserName() {
        return UserName;
    }
    public void setUserName(String userName) {
        UserName = userName;
    }
    public boolean getEnabled() {
        return Enabled;
    }
    public void setEnabled(boolean enabled) {
        Enabled = enabled;
    }

    
    @Override
    public String toString() {
        return "User [Password=" + Password + ", UserName=" + UserName + ", Enabled=" + Enabled + "]";
    }
    
    
}
